﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PermissionManager.Model.Migrations
{
    public partial class v4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CoverImageUrl",
                table: "StreamVideos");

            migrationBuilder.AddColumn<byte[]>(
                name: "CoverImage",
                table: "StreamVideos",
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "Image",
                table: "Players",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CoverImage",
                table: "StreamVideos");

            migrationBuilder.DropColumn(
                name: "Image",
                table: "Players");

            migrationBuilder.AddColumn<string>(
                name: "CoverImageUrl",
                table: "StreamVideos",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
