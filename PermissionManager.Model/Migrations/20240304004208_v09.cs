﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PermissionManager.Model.Migrations
{
    public partial class v09 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Players_StreamVideos_StreamVideoId",
                table: "Players");

            migrationBuilder.DropIndex(
                name: "IX_Players_StreamVideoId",
                table: "Players");

            migrationBuilder.DropColumn(
                name: "CoverImage",
                table: "StreamVideos");

            migrationBuilder.DropColumn(
                name: "HyperLink",
                table: "StreamVideos");

            migrationBuilder.DropColumn(
                name: "StreamVideoId",
                table: "Players");

            migrationBuilder.AddColumn<int>(
                name: "PlayerOneId",
                table: "StreamVideos",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PlayerTwoId",
                table: "StreamVideos",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PlayerOneId",
                table: "StreamVideos");

            migrationBuilder.DropColumn(
                name: "PlayerTwoId",
                table: "StreamVideos");

            migrationBuilder.AddColumn<byte[]>(
                name: "CoverImage",
                table: "StreamVideos",
                type: "varbinary(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "HyperLink",
                table: "StreamVideos",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StreamVideoId",
                table: "Players",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Players_StreamVideoId",
                table: "Players",
                column: "StreamVideoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Players_StreamVideos_StreamVideoId",
                table: "Players",
                column: "StreamVideoId",
                principalTable: "StreamVideos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
