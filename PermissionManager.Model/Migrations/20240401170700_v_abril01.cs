﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PermissionManager.Model.Migrations
{
    public partial class v_abril01 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Latitude",
                table: "Associations");

            migrationBuilder.DropColumn(
                name: "Longitude",
                table: "Associations");

            migrationBuilder.AddColumn<string>(
                name: "AddressUrl",
                table: "Associations",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AddressUrl",
                table: "Associations");

            migrationBuilder.AddColumn<double>(
                name: "Latitude",
                table: "Associations",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "Longitude",
                table: "Associations",
                type: "float",
                nullable: false,
                defaultValue: 0.0);
        }
    }
}
