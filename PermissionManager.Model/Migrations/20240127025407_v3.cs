﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PermissionManager.Model.Migrations
{
    public partial class v3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "StreamVideoId",
                table: "Players",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "StreamVideos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedDate = table.Column<DateTimeOffset>(nullable: true),
                    LiveUrl = table.Column<string>(nullable: true),
                    CoverImageUrl = table.Column<string>(nullable: true),
                    HyperLink = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StreamVideos", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Players_StreamVideoId",
                table: "Players",
                column: "StreamVideoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Players_StreamVideos_StreamVideoId",
                table: "Players",
                column: "StreamVideoId",
                principalTable: "StreamVideos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Players_StreamVideos_StreamVideoId",
                table: "Players");

            migrationBuilder.DropTable(
                name: "StreamVideos");

            migrationBuilder.DropIndex(
                name: "IX_Players_StreamVideoId",
                table: "Players");

            migrationBuilder.DropColumn(
                name: "StreamVideoId",
                table: "Players");
        }
    }
}
