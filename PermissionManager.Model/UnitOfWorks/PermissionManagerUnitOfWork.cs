﻿using PermissionManager.Core.BaseEntity;
using PermissionManager.Model.PermissionManagerContext.PermissionManager;
using PermissionManager.Model.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PermissionManager.Model.UnitOfWorks
{
    public class FDTMDbContextUnitOfWork : IUnitOfWork<IFDTMDbContext>
    {
        public IFDTMDbContext context { get; set; }
        public readonly IServiceProvider _serviceProvider;

        public FDTMDbContextUnitOfWork(IServiceProvider serviceProvider, IFDTMDbContext context)
        {
            _serviceProvider = serviceProvider;
            this.context = context;
        }

        public async Task<int> Commit()
        {
            var result = await context.SaveChangesAsync();
            return result;
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : class, IBaseEntity
        {
            return (_serviceProvider.GetService(typeof(TEntity)) ?? new BaseRepository<TEntity>(this)) as IRepository<TEntity>;
        }
    }
}
