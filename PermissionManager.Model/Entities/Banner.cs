﻿using PermissionManager.Core.BaseEntity;

namespace PermissionManager.Model.Entities
{
    public class Banner : BaseEntity
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public byte[] Image { get; set; }
    }
}
