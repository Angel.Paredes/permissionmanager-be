﻿using PermissionManager.Core.BaseEntity;

namespace PermissionManager.Model.Entities
{
    public class Association : BaseEntity
    {
        public string AddressUrl { get; set; }
        public string City { get; set; }
        public string Trainer { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }

    }
}
