﻿using PermissionManager.Core.BaseEntity;
using System;

namespace PermissionManager.Model.Entities
{
    public class Event : BaseEntity
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public byte[] Image { get; set; }
        public EventType Type { get; set; }
        public DateTime EventDate { get; set; }

    }

    public enum EventType
    {
        Event,
        News
    }
}
