﻿using PermissionManager.Core.BaseEntity;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace PermissionManager.Model.Entities.Permissions
{
    public class Player : BaseEntity
    {
        public string Name { get; set; }
        [NotMapped]
        public int Age { get; set; }
        public string LastName { get; set; }
        public int Ranking { get; set; }
        public DateTime BirdDate { get; set; }
        public byte[] Image { get; set; }
        public GenderType GenderType { get; set; }
        public string Nationality { get; set; }
    }
    public enum GenderType
    {
        Male,
        Famale
    }
}
