﻿using PermissionManager.Core.BaseEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace PermissionManager.Model.Entities.Permissions
{
    public class PermissionType : BaseEntity
    {
        public string Description { get; set; }
    }
}
