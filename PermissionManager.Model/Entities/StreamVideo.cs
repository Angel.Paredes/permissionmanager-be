﻿using PermissionManager.Core.BaseEntity;
using System;

namespace PermissionManager.Model.Entities
{
    public class StreamVideo : BaseEntity
    {
        public int? PlayerOneId { get; set; }
        public int? PlayerTwoId { get; set; }
        public string LiveUrl { get; set; }
        public DateTime StreamVideoDate { get; set; }

    }
}
