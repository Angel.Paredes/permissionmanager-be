﻿using PermissionManager.Core.BaseEntity;
using PermissionManager.Model.Entities.Permissions;

namespace PermissionManager.Model.Entities
{
    public class Ranking : BaseEntity
    {
        public int Point { get; set; }
        public int PlayerId { get; set; }
        public int? SecondPlayerId { get; set; }
        public int AssociationId { get; set; }

        public Association Association { get; set; }
        public Player Player { get; set; }
        public Player SecondPlayer { get; set; }

    }
}
