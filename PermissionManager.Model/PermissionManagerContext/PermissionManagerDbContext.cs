﻿using Microsoft.EntityFrameworkCore;
using PermissionManager.Core.BaseEntity;
using PermissionManager.Model.Entities;
using PermissionManager.Model.Entities.Permissions;
using PermissionManager.Model.PermissionManagerContext.PermissionManager;

namespace PermissionManager.Model.PermissionManagerContext
{
    public class FDTMDbContext : DbContext, IFDTMDbContext
    {
        public FDTMDbContext(DbContextOptions<FDTMDbContext> options) : base(options)
        {
        }
        public DbSet<Association> Associations { get; set; }
        public DbSet<Ranking> Rankings { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<StreamVideo> StreamVideos { get; set; }
        public DbSet<Banner> Banners { get; set; }

        public DbSet<T> GetDbSet<T>() where T : class, IBaseEntity => Set<T>();


    }
}
