﻿using Microsoft.Extensions.DependencyInjection;
using PermissionManager.Model.PermissionManagerContext;
using PermissionManager.Model.PermissionManagerContext.PermissionManager;
using PermissionManager.Model.UnitOfWorks;
using System;
using System.Collections.Generic;
using System.Text;

namespace PermissionManager.Model.IoC
{
    public static class ModelRegistry
    {
        public static void AddModelRegistry(this IServiceCollection services)
        {
            services.AddTransient<IFDTMDbContext, FDTMDbContext>();
            services.AddScoped<IUnitOfWork<IFDTMDbContext>, FDTMDbContextUnitOfWork>();
        }
    }
}
