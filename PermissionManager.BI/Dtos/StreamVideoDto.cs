﻿using PermissionManager.Core.BaseEntityDto;
using System;

namespace PermissionManager.BI.Dtos
{
    public class StreamVideoDto : BaseEntityDto
    {
        public int? PlayerOneId { get; set; }
        public int? PlayerTwoId { get; set; }
        public string LiveUrl { get; set; }
        public DateTime StreamVideoDate { get; set; }
    }
}
