﻿using PermissionManager.Core.BaseEntityDto;
using PermissionManager.Model.Entities;
using System;

namespace PermissionManager.BI.Dtos
{
    public class EventDto : BaseEntityDto
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public byte[] Image { get; set; }
        public EventType Type { get; set; }
        public DateTime EventDate { get; set; }
        public string imgBase64 { get; set; }

    }
}
