﻿using PermissionManager.Core.BaseEntityDto;

namespace PermissionManager.BI.Dtos
{
    public class AssociationDto : BaseEntityDto
    {
        public string AddressUrl { get; set; }
        public string City { get; set; }
        public string Trainer { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
    }
}
