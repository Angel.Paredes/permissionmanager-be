﻿using PermissionManager.Core.BaseEntityDto;
using PermissionManager.Model.Entities.Permissions;
using System;

namespace PermissionManager.BI.Dtos.Permissions
{
    public class PlayerDto : BaseEntityDto
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public DateTime BirdDate { get; set; }
        public int Ranking { get; set; }
        public byte[] Image { get; set; }
        public GenderType GenderType { get; set; }
        public string Nationality { get; set; }

    }
}
