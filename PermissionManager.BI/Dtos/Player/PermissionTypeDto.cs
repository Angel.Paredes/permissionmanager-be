﻿using PermissionManager.Core.BaseEntityDto;
using System;
using System.Collections.Generic;
using System.Text;

namespace PermissionManager.BI.Dtos.Permissions
{
    public class PermissionTypeDto : BaseEntityDto
    {
        public string Description { get; set; }
    }
}
