﻿using PermissionManager.Core.BaseEntityDto;

namespace PermissionManager.BI.Dtos
{
    public class BannerDto : BaseEntityDto
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public byte[] Image { get; set; }
        public string imgBase64 { get; set; }
    }
}
