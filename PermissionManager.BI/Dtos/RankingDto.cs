﻿using PermissionManager.BI.Dtos.Permissions;
using PermissionManager.Core.BaseEntityDto;

namespace PermissionManager.BI.Dtos
{
    public class RankingDto : BaseEntityDto
    {
        public int Point { get; set; }
        public int PlayerId { get; set; }
        public int? SecondPlayerId { get; set; }
        public int AssociationId { get; set; }

        public AssociationDto Association { get; set; }
        public PlayerDto Player { get; set; }
        public PlayerDto SecondPlayer { get; set; }

    }
}
