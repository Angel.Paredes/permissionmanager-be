﻿using AutoMapper;
using PermissionManager.BI.Dtos;
using PermissionManager.BI.Dtos.Permissions;
using PermissionManager.Model.Entities;
using PermissionManager.Model.Entities.Permissions;

namespace PermissionManager.BI.Mapping
{
    public class FDTMProfile : Profile
    {
        public FDTMProfile()
        {
            CreateMap<Player, PlayerDto>().ReverseMap();
            CreateMap<Ranking, RankingDto>().ReverseMap();
            CreateMap<StreamVideo, StreamVideoDto>().ReverseMap();
            CreateMap<Event, EventDto>().ReverseMap();
            CreateMap<Banner, BannerDto>().ReverseMap();
            CreateMap<Association, AssociationDto>().ReverseMap();

        }
    }
}
