﻿using PermissionManager.Core.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace PermissionManager.Core.BaseEntity
{
    public interface IBaseEntity : IBase
    {
        DateTimeOffset? CreatedDate { get; set; }
       
    }
}