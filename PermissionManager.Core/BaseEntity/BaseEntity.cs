﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PermissionManager.Core.BaseEntity
{
    public class BaseEntity : Base.Base, IBaseEntity
    {
        public virtual DateTimeOffset? CreatedDate { get; set; }
    }
}
