﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PermissionManager.Core.BaseDto
{
    public class BaseDto : IBaseDto
    {
        public virtual int? Id { get; set; }
    }
}
