﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PermissionManager.Core.BaseDto
{
    public interface IBaseDto
    {
        int? Id { get; set; }
    }
}
