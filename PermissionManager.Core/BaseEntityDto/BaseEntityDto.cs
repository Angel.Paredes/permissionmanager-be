﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PermissionManager.Core.BaseEntityDto
{
    public class BaseEntityDto : BaseDto.BaseDto, IBaseEntityDto
    {
        public DateTimeOffset? CreatedDate { get; set; }
    }
}
