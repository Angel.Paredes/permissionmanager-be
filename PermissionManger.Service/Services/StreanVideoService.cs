﻿using AutoMapper;
using PermissionManager.BI.Dtos;
using PermissionManager.Model.Entities;
using PermissionManager.Model.PermissionManagerContext.PermissionManager;
using PermissionManager.Model.Repositories;
using PermissionManager.Model.UnitOfWorks;
using PermissionManger.Services.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PermissionManger.Services.Services
{

    public interface IStreanVideoService : IEntityCRUDService<StreamVideo, StreamVideoDto>
    {
        List<StreamVideoDto> GetFourRecentNews();
    }

    public class StreanVideoService : EntityCRUDService<StreamVideo, StreamVideoDto>, IStreanVideoService
    {
        private IRepository<StreamVideo> _repository;
        public StreanVideoService(IMapper mapper, IUnitOfWork<IFDTMDbContext> uow) : base(mapper, uow)
        {
            _repository = uow.GetRepository<StreamVideo>();
        }

        public override async Task<StreamVideoDto> Save(StreamVideoDto entityDto)
        {
            entityDto.CreatedDate = DateTime.Now;
            return await base.Save(entityDto);
        }

        public List<StreamVideoDto> GetFourRecentNews()
        {
            List<StreamVideo> streamVideo = _repository.WhereAsNoTracking().OrderByDescending(x => x.CreatedDate).Take(4).ToList();

            return _mapper.Map<List<StreamVideoDto>>(streamVideo);
        }
    }

}
