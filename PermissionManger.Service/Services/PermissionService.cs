﻿using AutoMapper;
using PermissionManager.BI.Dtos.Permissions;
using PermissionManager.Model.Entities.Permissions;
using PermissionManager.Model.PermissionManagerContext.PermissionManager;
using PermissionManager.Model.Repositories;
using PermissionManager.Model.UnitOfWorks;
using PermissionManger.Services.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PermissionManger.Services.Services
{
    public interface IPlayerService : IEntityCRUDService<Player, PlayerDto>
    {
        List<PlayerDto> GetMenPlayers();
        List<PlayerDto> GetWomanPlayers();
    }

    public class PlayerService : EntityCRUDService<Player, PlayerDto>, IPlayerService
    {
        private IRepository<Player> _repository;
        public PlayerService(IMapper mapper, IUnitOfWork<IFDTMDbContext> uow) : base(mapper, uow)
        {
            _repository = uow.GetRepository<Player>();
        }

        public List<PlayerDto> GetMenPlayers()
        {
            IQueryable<Player> item = _repository
      .WhereAsNoTracking(x => x.GenderType == GenderType.Male)
    .Select(item =>
          item.BirdDate != null
              ? new Player
              {

                  Age = CalculateAge(item.BirdDate),
                  Name = item.Name,
                  LastName = item.LastName,
                  Image = item.Image,
                  Ranking = item.Ranking,
                  GenderType = GenderType.Male,
              }
              : item);


            return _mapper.Map<List<PlayerDto>>(item);

        }

        public List<PlayerDto> GetWomanPlayers()
        {
            IQueryable<Player> item = _repository.WhereAsNoTracking(x => x.GenderType == GenderType.Famale).Select(item =>
          item.BirdDate != null
              ? new Player
              {

                  Age = CalculateAge(item.BirdDate),
                  Name = item.Name,
                  LastName = item.LastName,
                  Image = item.Image,
                  Ranking = item.Ranking,
                  GenderType = GenderType.Famale,
              }
              : item);

            return _mapper.Map<List<PlayerDto>>(item);
        }


        public override async Task<PlayerDto> GetById(int id)
        {
            PlayerDto item = await base.GetById(id);

            if (item == null)
            {
                return new PlayerDto();
            }
            item.Age = CalculateAge(item.BirdDate);
            return item;
        }

        public override List<PlayerDto> Get()
        {
            List<PlayerDto> result = new List<PlayerDto>();

            foreach (var item in base.Get())
            {
                item.Age = CalculateAge(item.BirdDate);
                result.Add(item);
            }
            return result;

        }

        static int CalculateAge(DateTime birthDate)
        {
            // Get the current date
            DateTime currentDate = DateTime.Today;

            // Calculate the age
            int age = currentDate.Year - birthDate.Year;

            // Check if the birthday has occurred this year
            if (birthDate > currentDate.AddYears(-age))
            {
                age--;
            }

            return age;
        }


    }
}
