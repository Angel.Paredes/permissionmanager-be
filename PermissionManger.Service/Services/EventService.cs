﻿using AutoMapper;
using PermissionManager.BI.Dtos;
using PermissionManager.Model.Entities;
using PermissionManager.Model.PermissionManagerContext.PermissionManager;
using PermissionManager.Model.Repositories;
using PermissionManager.Model.UnitOfWorks;
using PermissionManger.Services.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PermissionManger.Services.Services
{
    public interface IEventService : IEntityCRUDService<Event, EventDto>
    {
        List<EventDto> GetFourRecentNews();
    }

    public class EventService : EntityCRUDService<Event, EventDto>, IEventService
    {
        private IRepository<Event> _repository;
        public EventService(IMapper mapper, IUnitOfWork<IFDTMDbContext> uow) : base(mapper, uow)
        {
            _repository = uow.GetRepository<Event>();
        }

        public override async Task<EventDto> Save(EventDto entityDto)
        {
            entityDto.CreatedDate = DateTime.Now;
            return await base.Save(entityDto);
        }
        public List<EventDto> GetFourRecentNews()
        {
            List<Event> streamVideo = _repository.WhereAsNoTracking(x => x.Type.Equals(EventType.News)).OrderByDescending(x => x.CreatedDate).Take(4).ToList();

            return _mapper.Map<List<EventDto>>(streamVideo);
        }
    }
}
