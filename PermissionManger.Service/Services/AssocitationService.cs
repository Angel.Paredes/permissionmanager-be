﻿using AutoMapper;
using PermissionManager.BI.Dtos;
using PermissionManager.Model.Entities;
using PermissionManager.Model.PermissionManagerContext.PermissionManager;
using PermissionManager.Model.Repositories;
using PermissionManager.Model.UnitOfWorks;
using PermissionManger.Services.Generic;

namespace PermissionManger.Services.Services
{
    public interface IAssociationService : IEntityCRUDService<Association, AssociationDto>
    {

    }

    public class AssociationService : EntityCRUDService<Association, AssociationDto>, IAssociationService
    {
        private IRepository<Association> _repository;
        public AssociationService(IMapper mapper, IUnitOfWork<IFDTMDbContext> uow) : base(mapper, uow)
        {
            _repository = uow.GetRepository<Association>();

        }
    }
}
