﻿using AutoMapper;
using PermissionManager.BI.Dtos;
using PermissionManager.Model.Entities;
using PermissionManager.Model.PermissionManagerContext.PermissionManager;
using PermissionManager.Model.Repositories;
using PermissionManager.Model.UnitOfWorks;
using PermissionManger.Services.Generic;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PermissionManger.Services.Services
{
    public interface IBannerService : IEntityCRUDService<Banner, BannerDto>
    {
    }

    public class BannerService : EntityCRUDService<Banner, BannerDto>, IBannerService
    {
        private IRepository<Banner> _repository;
        public BannerService(IMapper mapper, IUnitOfWork<IFDTMDbContext> uow) : base(mapper, uow)
        {
            _repository = uow.GetRepository<Banner>();
        }

        public override List<BannerDto> Get()
        {
            List<BannerDto> listv = base.Get().Select(x =>
            {
                return new BannerDto()
                {
                    Id = x.Id,
                    Title = x.Title,
                    Description = x.Description,
                    imgBase64 = Convert.ToBase64String(x.Image)
                };
            }).ToList();
            return listv;
        }
    }


}
