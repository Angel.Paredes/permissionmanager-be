﻿using AutoMapper;
using PermissionManager.BI.Dtos;
using PermissionManager.Model.Entities;
using PermissionManager.Model.PermissionManagerContext.PermissionManager;
using PermissionManager.Model.Repositories;
using PermissionManager.Model.UnitOfWorks;
using PermissionManger.Services.Generic;

namespace PermissionManger.Services.Services
{

    public interface IRankingService : IEntityCRUDService<Ranking, RankingDto>
    {
    }

    public class RankingService : EntityCRUDService<Ranking, RankingDto>, IRankingService
    {
        private IRepository<Ranking> _repository;
        public RankingService(IMapper mapper, IUnitOfWork<IFDTMDbContext> uow) : base(mapper, uow)
        {
            _repository = uow.GetRepository<Ranking>();
        }


    }
}
