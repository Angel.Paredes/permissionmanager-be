﻿using AutoMapper;
using PermissionManager.BI.Dtos.Permissions;
using PermissionManager.Model.Entities.Permissions;
using PermissionManager.Model.PermissionManagerContext.PermissionManager;
using PermissionManager.Model.UnitOfWorks;
using PermissionManger.Services.Generic;
using System;
using System.Collections.Generic;
using System.Text;

namespace PermissionManger.Services.Services
{
    public interface IPermissionTypeService : IEntityCRUDService<PermissionType, PermissionTypeDto>
    {
    }

    public class PermissionTypeService : EntityCRUDService<PermissionType, PermissionTypeDto>, IPermissionTypeService
    {
        public PermissionTypeService(IMapper mapper, IUnitOfWork<IFDTMDbContext> uow) : base(mapper, uow)
        {

        }

    }
}
