﻿using Microsoft.Extensions.DependencyInjection;
using PermissionManger.Services.Services;

namespace PermissionManger.Services.IoC
{
    public static class ServicesRegistry
    {
        public static void AddServicesRegistry(this IServiceCollection services)
        {
            services.AddScoped<IRankingService, RankingService>();
            services.AddScoped<IAssociationService, AssociationService>();
            services.AddScoped<IPermissionTypeService, PermissionTypeService>();
            services.AddScoped<IPlayerService, PlayerService>();
            services.AddScoped<IStreanVideoService, StreanVideoService>();
            services.AddScoped<IEventService, EventService>();
            services.AddScoped<IBannerService, BannerService>();

        }
    }
}
