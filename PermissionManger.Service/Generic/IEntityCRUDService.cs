﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PermissionManger.Services.Generic
{
    public interface IEntityCRUDService<TEntity, TEntityDto> where TEntity : class where TEntityDto : class
    {
        Task<TEntityDto> GetById(int id);
        Task<TEntityDto> Save(TEntityDto entityDto);
        Task<TEntityDto> Update(int id, TEntityDto entityDto);
        Task<TEntityDto> Delete(int id);
        List<TEntityDto> Get();
    }
}
