using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using PermissionManager.Core.Models;
using PermissionManager.Model.IoC;
using PermissionManager.Model.PermissionManagerContext;
using PermissionManger.Services.IoC;
using System;
using System.IO;
using System.Reflection;

namespace PermissionManager.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            #region Adding Settings Sections
            services.Configure<AppSetting>(Configuration.GetSection("AppSetting"));
            services.Configure<ConnectionString>(Configuration.GetSection("ConnectionString"));
            #endregion


            #region CORS
            var appSettings = Configuration.GetSection("AppSetting").Get<AppSetting>();
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAllPolicy",
                      builder =>
                      {
                          builder
                                 .WithOrigins(appSettings.ClientUrls)
                                 .AllowAnyHeader()
                                 .AllowAnyMethod()
                                 .AllowCredentials();
                          builder.SetIsOriginAllowed(x => true);

                      });
            });
            #endregion

            #region IoC Registry
            services.AddModelRegistry();
            services.AddServicesRegistry();
            #endregion

            #region ContextConfiguration
            services.AddDbContext<FDTMDbContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
            });
            #endregion

            #region Adding External Libs
            //Register AutoMapper
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            #endregion

           services.AddControllers().AddNewtonsoftJson();
            services.AddApiVersioning(options =>
            {
                options.DefaultApiVersion = new ApiVersion(1, 0);
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.ReportApiVersions = true;
            });
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo { Title = "Your API", Version = "v1" });

                // Add support for XML comments
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                options.IncludeXmlComments(xmlPath);
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
                  Path.Combine(env.ContentRootPath, "")),
                RequestPath = ""
            });
            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseCors("AllowAllPolicy");
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            app.UseSwagger();
            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "Your API v1");
                options.RoutePrefix = "api/app/swagger"; // Set the Swagger UI at the root URL
            });

        }
    }
}
