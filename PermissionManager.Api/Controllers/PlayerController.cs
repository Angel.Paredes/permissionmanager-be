﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PermissionManager.BI.Dtos.Permissions;
using PermissionManager.Model.Entities.Permissions;
using PermissionManger.Services.Services;

namespace PermissionManager.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PlayerController : BaseController<Player, PlayerDto>
    {
        private readonly IPlayerService _playerService;
        public PlayerController(IPlayerService playerService, IMapper mapper) : base(playerService, mapper)
        {
            _playerService = playerService;
        }

        [Route("ManPlayer")]
        [HttpGet]
        public IActionResult GetManPlayer()
        {
            return Ok(_playerService.GetMenPlayers());
        }

        [Route("WomanPlayer")]

        [HttpGet]
        public IActionResult GetWomanPlayer()
        {
            return Ok(_playerService.GetWomanPlayers());
        }
    }
}