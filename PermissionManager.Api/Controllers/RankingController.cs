﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PermissionManager.BI.Dtos;
using PermissionManager.Model.Entities;
using PermissionManger.Services.Services;

namespace PermissionManager.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RankingController : BaseController<Ranking, RankingDto>
    {
        public RankingController(IRankingService rankingService, IMapper mapper) : base(rankingService, mapper)
        {
        }
    }
}
