﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PermissionManager.BI.Dtos;
using PermissionManager.Model.Entities;
using PermissionManger.Services.Services;

namespace PermissionManager.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class StreamVideoController : BaseController<StreamVideo, StreamVideoDto>
    {
        private readonly IStreanVideoService _streanVideoService;
        public StreamVideoController(IStreanVideoService streanVideoService, IMapper mapper) : base(streanVideoService, mapper)
        {
            _streanVideoService = streanVideoService;
        }
        [Route("RecentVideos")]
        [HttpGet]
        public IActionResult HandleRecentsVideos()
        {
            return Ok(_streanVideoService.GetFourRecentNews());
        }


    }
}
