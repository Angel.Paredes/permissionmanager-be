﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PermissionManager.BI.Dtos;
using PermissionManager.Model.Entities;
using PermissionManger.Services.Services;

namespace PermissionManager.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AssociationController : BaseController<Association, AssociationDto>
    {
        public AssociationController(IAssociationService associationService, IMapper mapper) : base(associationService, mapper)
        {
        }
    }
}
