﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PermissionManager.BI.Dtos;
using PermissionManager.Model.Entities;
using PermissionManger.Services.Services;

namespace PermissionManager.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EventController : BaseController<Event, EventDto>
    {
        private readonly IEventService _eventService;
        public EventController(IEventService eventService, IMapper mapper) : base(eventService, mapper)
        {
            _eventService = eventService;
        }
        [Route("RecentsNews")]
        [HttpGet]
        public IActionResult HandleRecentsNews()
        {
            return Ok(_eventService.GetFourRecentNews());
        }

    }
}
