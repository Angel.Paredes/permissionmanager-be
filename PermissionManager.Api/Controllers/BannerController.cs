﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PermissionManager.BI.Dtos;
using PermissionManager.Model.Entities;
using PermissionManger.Services.Services;

namespace PermissionManager.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BannerController : BaseController<Banner, BannerDto>
    {
        public BannerController(IBannerService bannerService, IMapper mapper) : base(bannerService, mapper)
        {
        }
    }
}
